<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl | Contact Us</title>
        <link rel="stylesheet" type="text/css" href="../css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="content">
            <a href="../index.php">
            <div id="banner">
                <?php include '../includes/banner.php'; ?> 
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <ul>
                <li><a href="../index.php">Home </a></li>
                <li><a href="../files/about.php">About </a></li>
                <li><a href="../files/gallery.php">Gallery </a></li>
                <li><a href="../files/order.php">Order Online</a></li>
                <li><b><a href="../files/contact.php">Contact Us</a></b></li>
                </ul>
                <hr>
            </div> <!--end nav div-->
            
            <div id="contactform">
                <h2>Contact Us</h2>
                <form action="../files/contactsent.php" method="POST">
                <div>
                    <label for="name" >Name:</label>
                    <input id="name" type="text" name="name" />
                </div>
                <div>
                    <label for="mail">E-mail:</label>
                    <input id="mail" type="email" name="email" />
                </div>
                <div>
                    <label for="msg">Message:</label>
                </div>
                <div>
                    <textarea id="msg" name="msg" placeholder="Please enter your message here."></textarea>
                </div>
                <div class="button">
                    <button type="submit" name="submit" id="submit">Send Message</button>
                </div>
                </form>
            </div>
            
            <div id="footer">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="../img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>