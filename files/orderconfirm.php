<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl | Order Online</title>
        <link rel="stylesheet" type="text/css" href="../css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="content">
            <a href="../index.php">
            <div id="banner">
                <?php include '../includes/banner.php'; ?> 
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <ul>
                <li><a href="../index.php">Home </a></li>
                <li><a href="../files/about.php">About </a></li>
                <li><a href="../files/gallery.php">Gallery </a></li>
                <li><a href="../files/order.php">Order Online</a></li>
                <li><a href="../files/contact.php">Contact Us</a></li>
                </ul>
                <hr>
            </div> <!--end nav div-->
            
            <div id="orderconfirm">
                <?php
                    if (isset($_POST['submit'])) {
                        $name = $_POST['name'];
                        $email = $_POST['email'];
                        $product = $_POST['prod'];
                        $quantity = $_POST['quant'];

                        if (empty($name) || empty($email)) {
                            echo "<p>Form is incomplete.</p>";
                            echo '<p><a href="../files/order.php">Return to Form</a></p>';
                        } else {
                            $company = "gaw85@cornell.edu";
                            $subject = "Order Placed";
                            $subject2 = "Thank you for your order!";
                            $message = $name . " placed an order of: " . $product . " with quantity: " . $quantity;
                            $message2 = "Dear " . $name . "," . "\n" . "\n" . "Thank you for placing an order with The Sugar Bowl." . "\n" . $product . " with quantity: " . $quantity . "\n" . "Please mail a check made out to The Sugar Bowl to 109 Triphammer Rd, Ithaca, NY 14850 with the total amount." . "\n" . "\n" . "The Sugar Bowl";

                            $headers = "From: " . $company;
                            
                            mail($company,$subject,$message,$headers);
                            mail($email,$subject2,$message2,"The Sugar Bowl");
                            echo "<p>Thank you for your order. You will receive an email confirmation shortly including instructions for payment.</p>";
                        }
                    } else {
                        echo "<p>Form was not submitted</p>";
                    }
                ?>
            </div>
            
            <div id="footer">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="../img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>