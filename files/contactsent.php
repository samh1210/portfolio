<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl | Contact Us</title>
        <link rel="stylesheet" type="text/css" href="../css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="content">
            <a href="../index.php">
            <div id="banner">
                <?php include '../includes/banner.php'; ?> 
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <ul>
                <li><a href="../index.php">Home </a></li>
                <li><a href="../files/about.php">About </a></li>
                <li><a href="../files/gallery.php">Gallery </a></li>
                <li><a href="../files/order.php">Order Online</a></li>
                <li><a href="../files/contact.php">Contact Us</a></li>
                </ul>
                <hr>
            </div> <!--end nav div-->
            
            <div id="contactsent">
                <?php
                    if (isset($_POST['submit'])) {
                        $name = $_POST['name'];
                        $email = $_POST['email'];
                        $messagesub = $_POST['msg'];

                        if (empty($name) || empty($email) || empty($messagesub)) {
                            echo "<p>Form is incomplete.</p>";
                            echo '<p><a href="../files/contact.php">Return to Form</a></p>';
                        } else {
                            $company = "gaw85@cornell.edu";
                            $subject = "Customer Contact from:" . $email;
                            $subject2 = "Thank you for contacting The Sugar Bowl!";
                            $message = $name . " contacted us with the message: " . $messagesub;
                            $message2 = "Dear " . $name . "," . "\n" . "\n" . "Thank you for contacting The Sugar Bowl." . "\n" . "Message: " . $messagesub . "\n" . "You should expect a response within the next 24-48 hours. Have a Great Day!" . "\n". "\n" . "The Sugar Bowl";
                            
                            mail($company,$subject,$message);
                            mail($email,$subject2,$message2);
                            echo "<p>Thank you for contacting The Sugar Bowl. You should expect a response within 24-48 hours. You will receive an email shortly with a copy of your message.</p>";
                        }
                    } else {
                        echo "<p>Form was not submitted</p>";
                    }
                ?>
            </div>
            
            <div id="footer">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="../img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>