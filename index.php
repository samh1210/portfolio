<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl</title>
        <link rel="stylesheet" type="text/css" href="css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript">
            /*** 
                Simple jQuery Slideshow Script
                Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)
            ***/

            function slideSwitch() {
                var $active = $('#slideshow IMG.active');

                if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

                // use this to pull the images in the order they appear in the markup
                var $next =  $active.next().length ? $active.next()
                    : $('#slideshow IMG:first');

                // uncomment the 3 lines below to pull the images in random order

                // var $sibs  = $active.siblings();
                // var rndNum = Math.floor(Math.random() * $sibs.length );
                // var $next  = $( $sibs[ rndNum ] );


                $active.addClass('last-active');

                $next.css({opacity: 0.0})
                    .addClass('active')
                    .animate({opacity: 1.0}, 1000, function() {
                        $active.removeClass('active last-active');
                    });
            }

            $(function() {
                setInterval( "slideSwitch()", 5000 );
            });
        </script>
    
    </head>
    <body>
        <div class="content">
            <a href="index.php">
            <div id="banner">
                <img src="img/logo.png" width="800" alt="The Sugar Bowl | Logo">
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <?php include 'includes/navindex.php'; ?>
                <hr>
            </div> <!--end nav div-->
            
            <div id="slidecontent">
                <div id="slideshow">
                    <img src="img/widescreen1.jpg" alt="Slide1" class="active" />
                    <img src="img/widescreen2.jpg" alt="Slide2" />
                    <img src="img/widescreen3.jpg" alt="Slide3" />
                </div> <!--end slideshow div -->
            </div> <!--end slidecontent div -->
            
            <div id="desc">
                    <p>The Sugar Bowl is a custom cookie and baked goods company started by the Winant family in 2010. The Sugar Bowl wants to cater for your special event! Be sure to check out their previous work in the gallery and on Instagram!</p>
                </div> <!-- END DESC DIV -->
            
            <div id="piclinks">
                <a href="files/about.php"><img src="img/SugarBowl8.jpg" alt="The Sugar Bowl | About"></a>
                <a href="files/gallery.php"><img src="img/SugarBowl3.jpg" alt="The Sugar Bowl | Gallery"></a>
                <a href="files/order.php"><img src="img/SugarBowl27.jpg" alt="The Sugar Bowl | Order"></a>
            </div>
            
            <div id="footer">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>