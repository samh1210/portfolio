<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl | Order Online</title>
        <link rel="stylesheet" type="text/css" href="../css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="content">
            <a href="../index.php">
            <div id="banner">
                <?php include '../includes/banner.php'; ?> 
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <ul>
                <li><a href="../index.php">Home </a></li>
                <li><a href="../files/about.php">About </a></li>
                <li><a href="../files/gallery.php">Gallery </a></li>
                <li><b><a href="../files/order.php">Order Online</a></b></li>
                <li><a href="../files/contact.php">Contact Us</a></li>
                </ul>
                <hr>
            </div> <!--end nav div-->
            
            <div id="orderform">
                <h2>Order Online</h2>
                <form action="../files/orderconfirm.php" method="POST">
                <div>
                    <label for="name" >Name:</label>
                    <input id="name" type="text" name="name" />
                </div>
                <div>
                    <label for="mail">E-mail:</label>
                    <input id="mail" type="email" name="email" />
                </div>
                <div>
                    <label for="prod">Product:</label>
                    <select id="prod" name="prod">
                      <optgroup label="Cookies ($25/tray)">
                        <option value="Sugar Cookies" selected="selected">Sugar Cookies</option>
                        <option value="Butterfly Cookies">Butterfly Cookies</option>
                        <option value="Neapolitan Cookies">Neapolitan Cookies</option>
                        <option value="Thimble Cookies">Thimble Cookies</option>
                        <option value="Oatmeal Chocolate Cookies">Oatmeal Chocolate Cookies</option>
                        <option value="Shortbread Cookies">Shortbread Cookies</option>
                        <option value="Chocolate Crescent Cookies">Chocolate Crescent Cookies</option>
                        <option value="Snowflake Cookies">Snowflake Cookies</option>
                        <option value="Custom Sugar Cookies">Custom Sugar Cookies</option>
                      </optgroup>
                      <optgroup label="Cupcakes ($14/Dozen) ">
                        <option value="Vanilla Cupcakes">Vanilla</option>
                        <option value="Chocolate Cupcakes">Chocolate</option>
                        <option value="Cookies & Cream Cupcakes">Cookies and Cream</option>
                        <option value="Red Velvet Cupcakes">Red Velvet</option>
                      </optgroup>
                      <optgroup label="Treats ($25/tray)">
                        <option value="Pecan Butter Balls">Pecan Butter Balls</option>
                        <option value="Ginger Snaps">Ginger Snaps</option>
                        <option value="Burban Balls">Burban Balls</option>
                        <option value="Cheesecake Brownies">Cheesecake Brownies</option>
                        <option value="Red Velvet Brownies">Red Velvet Brownies</option>
                      </optgroup>
                    </select>
                </div>
                <div>
                    <label for="quant">Quantity:</label>
                    <select id="quant" name="quant">
                        <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
                <div class="button">
                    <button type="submit" name="submit" id="submit">Send Order</button>
                </div>
                </form>
            </div>
            
            <div id="footer">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="../img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>