<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl | Gallery</title>
        <link rel="stylesheet" type="text/css" href="../css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    </head>
    <body>
        <div class="content">
            <a href="../index.php">
            <div id="banner">
                <?php include '../includes/banner.php'; ?> 
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <ul>
                <li><a href="../index.php">Home </a></li>
                <li><a href="../files/about.php">About </a></li>
                <li><b><a href="../files/gallery.php">Gallery </a></b></li>
                <li><a href="../files/order.php">Order Online</a></li>
                <li><a href="../files/contact.php">Contact Us</a></li>
                </ul>
                <hr>
            </div> <!--end nav div-->
            
            <div id="images">
                <div id="notes">
                <ul>
                    <li>
                        <a href="#">
                            <img src="../img/SugarBowl12.jpg" alt="Cookies">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="../img/SugarBowl13.jpg" alt="Cookies">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="../img/SugarBowl16.jpg" alt="Cookies">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="../img/SugarBowl26.jpg" alt="Cookies">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="../img/SugarBowl21.jpg" alt="Cookies">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="../img/SugarBowl22.jpg" alt="Cookies">
                        </a>
                    </li>
                </ul>
                </div> <!-- end notes div -->
            </div> <!--end images div-->
            
            <div id="gallbanner">
                <img src="../img/cookiebanner.jpg" alt="The Sugar Bowl | Cookie Gallery">
            </div>
            
            <div id="footergallery">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="../img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>