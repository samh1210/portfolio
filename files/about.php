<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Sugar Bowl | About</title>
        <link rel="stylesheet" type="text/css" href="../css/sugar.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="content">
            <a href="../index.php">
            <div id="banner">
                <?php include '../includes/banner.php'; ?> 
            </div> <!--end banner div-->
            </a>
            
            <div id="nav">
                <hr>
                <ul>
                <li><a href="../index.php">Home </a></li>
                <li><b><a href="../files/about.php">About </a></b></li>
                <li><a href="../files/gallery.php">Gallery </a></li>
                <li><a href="../files/order.php">Order Online</a></li>
                <li><a href="../files/contact.php">Contact Us</a></li>
                </ul>
                <hr>
            </div> <!--end nav div-->
    
            <div id="about">
                <h1>About The Sugar Bowl</h1>
                <img id="grace" src="../img/graceandsis.jpeg" alt="The Sugar Bowl | Founders">
                <div id="aboutright">
                    <div id="abouttext">
                        The Sugar Bowl was started by my sister, Sara Winant (under the guidance of our mom, Wendy) to help pay for college in 2010. Our family had always been known for our Christmas cookies, so we decided, why not, let’s start making a profit. Once my sister left for college, it was left to me, Grace, to take over. I went a little rouge and started making birthday cakes and different desserts, which expanded our customer base. To this day, our most popular dessert is our customized sugar cookies that we make for different occasions like baby announcements and graduation parties. Looking ahead, the Sugar Bowl continues to operate even though I am at college. In fact, The Sugar Bowl has catered for various events right here on campus - for example, Kappa Delta’s Shamrock Bash and their campus Girl Scout event. I hope to expand the business even further on campus and work with club events, sports teams, and Greek life events. Have a sweet day!
                    </div>
                    <div id="aboutpics">
                        <img class="littleabouts" src="../img/SugarBowl2.jpg" alt="The Sugar Bowl | About">
                        <img class="littleabouts" src="../img/SugarBowl10.jpg" alt="The Sugar Bowl | About">
                        <img class="littleabouts" src="../img/SugarBowl15.jpg" alt="The Sugar Bowl | About">
                    </div> <!--end aboutpics div-->
                </div> <!--end aboutright div -->
            </div> <!--end about div-->
            
            <div id="footerabout">
                <p>Copyright &copy; 2015 The Sugar Bowl • <a href="http://www.instagram.com/the_sugar_bowl"><img src="../img/insta.jpg" class="insta" alt="insta"></a></p>
            </div><!--end footer div -->
            
        </div> <!--end content div-->
    </body>
</html>